This repo contains my steps to setup a tezos baking VPS

# get a VPS
# install debian 9
# install tezos
https://github.com/tezoscommunity/FAQ/wiki/Build-a-zeronet-node-on-Debian-9
# Harden
## Firewall
* sudo ufw default deny outgoing
* sudo ufw default deny incoming
* sudo ufw allow ssh
* sudo ufw allow 9732
* sudo ufw enable

## SSH
* apt install fail2ban
* Pubkey auth

# Commands

## To see progress run the following command which shows the head block known so far:
`./tezos-client rpc call /blocks/head with '{}'`  

Look for the `timestamp` value in the output from that. When that value gets to within a minute or so of the current date and time then your node is synced.


## Check out the Alphanet constants
`./tezos-client rpc call /blocks/head/proto/constants`  

Check for the `endorsement_security_deposit` and `block_security_deposit` keys of the JSON record. The value is in µtez, one millionth of a tezzie. In the alphanet, the current value is set to 512tz per block and 64tz per endorsement. If you run out of funds, you will not be able to bake.